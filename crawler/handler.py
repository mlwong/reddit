# IMPORTS
import praw
import multiprocessing as mp
import logging

import requests
import requests.auth

import time
from datetime import datetime

import worker

#SUBREDDITS = 'Politics+news+worldnews'
SUBREDDITS = 'Politics'
DRY_RUN = True

def spawn_worker(sub_id, sub_num):
    w = worker.Worker(sub_id, dry_run = DRY_RUN, sub_num = sub_num)
    w.run()

def main():
    sub_count = 0

    logging.basicConfig(filename='../logs/reddit.log', level=logging.INFO)
    logging.info('initialize PRAW Reddit object')

    tracked_posts = []

    with open('.user_info', 'r') as f:
        account_info = [line.rstrip('\n') for line in f]
    account_info = account_info[0].split(' ')
    self._reddit = praw.Reddit(client_id=account_info[2],
                         client_secret=account_info[3],
                         password=account_info[1],
                         user_agent='crawler',
                         username=account_info[0])

    print('bootstrap')
    logging.info('bootstrap with first 10 new listings')
    listing = reddit.subreddit(SUBREDDITS).new(limit=10)

    for sub in listing:
        tracked_posts.append(sub.id)
        logging.info('[{}, {}] starting process'.format(sub_count, sub.id))
        p = mp.Process(target=spawn_worker, kwargs={'sub_id': sub.id, 'sub_num': sub_count})
        p.start()
        #t = threading.Thread(target=spawn_worker, kwargs={'sub_id': sub.id, 'sub_num': sub_count})
        #t.start()
        sub_count += 1

    while True:
        time.sleep(60)
        logging.info('number of processes: {}'.format(len(mp.active_children())))
        if len(mp.active_children()) < 500:
            logging.info('getting newest 10 posts')
            try:
                listing = reddit.subreddit(SUBREDDITS).new(limit=10)
            except:
                reddit = praw.Reddit(client_id='RT3i8N5uuvsgHw',
                                     client_secret='V3yKuuZQVGrPlA8rZnxo7KHuv4Y',
                                     password='mlwong2',
                                     user_agent='crawler',
                                     username='mlwong2',
                                     duration='permanent')
                listing = reddit.subreddit(SUBREDDITS).new(limit=10)
            for sub in listing:
                if not sub.id in tracked_posts:
                    try:
                        logging.info('found new post {}'.format(sub.id))
                        tracked_posts.append(sub.id)
                        p = mp.Process(target=spawn_worker, kwargs={'sub_id': sub.id, 'sub_num': sub_count})
                        p.start()
                        #t = threading.Thread(target=spawn_worker, kwargs={'sub_id': sub, 'sub_num': sub_count})
                        #t.start()
                        sub_count += 1
                    except:
                        print('unable to add new post')
        else:
            logging.info('too many threads, not getting new posts')

    logging.error('exited infinite loop')

if __name__ == '__main__':
    main()
