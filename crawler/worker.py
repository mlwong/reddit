# IMPORTS
import praw
import logging

import pandas as pd

import psycopg2
import psycopg2.extras
from getpass import getpass
import uuid

import time
from datetime import datetime
import random

class Worker:
    def __init__(self, sub_id, dry_run = False, sub_num = -1):
        self._sub_id = sub_id
        self._sub_num = sub_num

        # LOGGING
        logging.basicConfig(filename='../logs/reddit.log', level=logging.INFO)

        # PRAW SETUP
        with open('.user_info', 'r') as f:
            account_info = [line.rstrip('\n') for line in f]
        account = random.randint(0,1)
        account_info = account_info[account].split(' ')
        self._reddit = praw.Reddit(client_id=account_info[2],
                             client_secret=account_info[3],
                             password=account_info[1],
                             user_agent='crawler',
                             username=account_info[0])

        # CONSTANTS
        # time intervals in minutes
        self._TIME_INTERVALS = (5, 10, 15, 20, 30, 40, 50, 60, 90, 120,
                      150, 180, 240, 300, 360, 420, 480, 540, 600,
                      720, 840, 960, 1080, 1200, 1320, 1440)
        self._INSERT_STMT = 'INSERT INTO posts VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'
        self._DRY_RUN = dry_run

    # HELPER FUNCTIONS
    def __get_time_info(self, unix_time):
        age = int(time.time()) - unix_time
        time_bin = 0
        for ti in self._TIME_INTERVALS:
            if age < ti*60:
                break
            time_bin += 1
        return(age, time_bin)

    def __get_delay_timestamp(self, delay):
        return datetime.fromtimestamp(int(datetime.now().timestamp()+delay))

    def __get_sub_info(self):
        submission = self._reddit.submission(id=self._sub_id)
        top_100 = self._reddit.subreddit(str(submission.subreddit)).hot(limit=100)

        is_frontpage = False
        position = 0
        for post in top_100:
            position += 1
            if post.id == submission.id:
                is_frontpage = True
                break
        age, time_bin = self.__get_time_info(submission.created_utc)
        sub_info = (psycopg2.extras.UUID_adapter(uuid.uuid4()),
                    submission.id,
                    submission.url,
                    submission.subreddit.title,
                    submission.author.name,
                    submission.created_utc,
                    age,
                    time_bin,
                    submission.score,
                    submission.upvote_ratio,
                    submission.title,
                    submission.selftext,
                    submission.num_comments,
                    True if submission.gilded else False,
                    submission.author.comment_karma,
                    submission.author.link_karma,
                    position if is_frontpage else 0)
        return(sub_info)

    def __get_delta_t(self, unix_time, age):
        sub_age = int(time.time()) - unix_time
        for ti in (time*60 for time in self._TIME_INTERVALS):
            if sub_age < ti:
                time_delay = ti - sub_age
                break
        return(time_delay)

    def __insert_sub_info(self, sub_info):
        db_conn = psycopg2.connect("dbname='reddit' host='192.168.0.5' user='max' password='reddit'")
        with db_conn.cursor() as cur:
            try:
                cur.execute(self._INSERT_STMT, sub_info)
                db_conn.commit()
            except Exception as e:
                logging.error('exception in insert_sub_info(): {}'.format(str(e)))
                db_conn.rollback()
        db_conn.close()

    def run(self):
        try:
            sub_info = self.__get_sub_info()
        except ValueError as e:
            logging.error('[{}, {}] error: {}'.format(self._sub_num, self._sub_id, repr(err)))
            return

        if not self._DRY_RUN: self.__insert_sub_info(sub_info)

        # while not older than last time interval
        while sub_info[6] < self._TIME_INTERVALS[-1]*60:
            delta_t = self.__get_delta_t(sub_info[5], sub_info[6])
            logging.info('[{}, {}] delta_t: {}'.format(self._sub_num, self._sub_id, delta_t))
            time.sleep(delta_t)
            try:
                sub_info = self.__get_sub_info()
            except ValueError as e:
                logging.error('[{}, {}] error: {}'.format(self._sub_num, self._sub_id, repr(err)))
                return
            if not self._DRY_RUN: self.__insert_sub_info(sub_info)
        logging.info('[{}, {}] old post, ending process'.format(self._sub_num, self._sub_id))
