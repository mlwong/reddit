\documentclass[a4paper, 11pt]{article}
\usepackage{comment} % enables the use of multi-line comments (\ifx \fi) 
\usepackage{fullpage} % changes the margin
\usepackage{url}
\usepackage{enumitem}
\usepackage{graphicx}
\graphicspath{ {../figures/} }
\usepackage{subcaption}
\usepackage{float}

\begin{document}
\noindent
\large\textbf{Predicting Reddit Front Page Posts} \hfill \textbf{Maxwell Wong} \\
\normalsize STATS 170AB \hfill mlwong2@uci.edu\\
Prof. Smyth \hfill 6/10/18 \\
Prof. Carey

\section*{1. Introduction and Problem Statement}
Reddit is an internet forum with millions of daily users who "upvote" and "downvote" posts to increase or decrease post visibility. When a post is upvoted enough, it rises to the top of the list of posts, reaching the front page. Oftentimes multiple posts are made with very similar information, such as a link to the same news article, and one post will rise to the front page, where the other will barely be seen.

This project will develop a system to predict whether or not a post will reach its particular subreddit's front page (defined as top 50 posts for the purpose of this project) within a 24 hour timespan. There are two distinct systems in this project, (1) web crawler to collect data for each post of interest over a 24 hour period from its initial posting via Reddit’s API, (2) training and application of a model to make predictions. Predictions were able to be made with \url{~}90\% accuracy.

\section*{2. Related Work}
A large portion of this project involves collecting data. There exist publicly available Reddit datasets, but they only include a single time point for each post, whereas this project requires a time series for each post. Data collection was done using Reddit's API through Python Reddit API Wrapper (PRAW). Classification models use off the shelf implementations from scikit-learn, along with numpy, pandas, multiprocessing,  and imbalanced-learn.

\section*{3. Data Sets}
While there are datasets on Reddit data, none contained time series data for each post, which is required to make the predictions necessary for this project. Data was collected for this project using PRAW to access Reddit's OAuth2 API. Because of the sheer volume of posts to Reddit, data was collected only from the r/Politics subreddit. r/Politics was chosen because (1) there are a large number of posts, (2) there are a large number of users to provide a large number of upvotes and downvotes, and (3) all posts are links to external news sites. The data set contains \url{~}2500 posts worth of data, or \url{~}67,500 total data points (numbers are approximate because data collection is ongoing). The features collected are composed of the following:

\begin{enumerate}[itemsep=-2mm]
	\item \textbf{post\_id}: unique post identifier
	\item \textbf{url}: url of the link being posted
	\item \textbf{subreddit}: subreddit that the post came from (initially, multiple subreddits were used)
	\item \textbf{username}: username that uploaded a post
	\item \textbf{created\_at}: UNIX timestamp of when a post was uploaded
	\item \textbf{age}: age of a post in seconds
	\item \textbf{time\_bin}: time bin under which the data point falls into
	\item \textbf{score}: Reddit's obfuscated estimation of a score
	\item \textbf{upvote\_ratio}: the approximate ratio of upvotes and downvotes
	\item \textbf{title}: text used as the post's title
	\item \textbf{selftext}: text body of the post, null if there is a link
	\item \textbf{num\_comments}: the number of comments made on the post
	\item \textbf{gilded}: boolean, whether or not the post has been gilded (support beyond a simple upvote shown by a user purchasing "gold" for the poster)
	\item \textbf{user\_comment\_karma}: the total number of upvotes a user's comments have garnered
	\item \textbf{user\_link\_karma}: the total number of upvotes a user's posts have garnered
	\item \textbf{frontpage\_pos}: the position of a post on the frontpage (1-100), 0 otherwise
\end{enumerate}

For each post, data was collected over 24 hours in 26 time bins on an exponential schedule shown in Figure 1. This was done, because posts tend to reach the front page very quickly, and once they get to the front page, they often stay on the front page for many hours. Having a higher data density closer to the beginning of a post's lifespan allows for more potential signal in the most critical part of a post's journey to the front page.

\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{time_bins}
\caption{The time in hours at which data is collected for each of the 27 time bins.}
\end{figure}

\section*{4. Description of Overall Technical Approach}
\subsection*{Part 1: Data collection}
The data collection system is implemented using Python's multiprocessing capabilities in tandem with PRAW. This system has two main components, a handler that continuously checks for new posts, and workers that handle all of the data collection for a post.

The handler maintains a list of posts that are being tracked, and has an infinite loop on a one minute timer in which it checks the newest 10 posts for new posts that are not yet being tracked. New posts are submitted on r/Politics roughly every 1-5 minutes, so checking the top 10 posts every minute ensures no posts will be missed. When the handler finds a new post, it passes off the post\_id to a new worker process.

A worker exists for each post being tracked, and is terminated once all 27 data points for a post have been collected. When a data point is collected, it calculates the time until the next data point, and then sleeps for that amount of time, collects data again, and repeats. Collecting a data point involves making a request to Reddit's API with PRAW, sorting through the response to extract relevant features, and then inserting the resulting feature values into the PostgreSQL database.

Because PRAW is both not thread safe and PRAW requests are thread blocking, entire processes had to be used in order to properly isolate PRAW instances from one another as opposed to more lightweight threads. If a single process and single PRAW instance is used to make all of the requests, the system begins overloading at less than 100 posts. Because of these constraints and the relatively larger memory requirements of processes, only 500 posts were able to be reliably tracked at any given moment due to hardware limitations (i7-2600k, 8GB RAM). The system was memory limited as opposed to CPU or network limited, so theoretically a machine with more memory could handle tracking more posts.

\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{workflow}
\caption{High level overview of workflow.}
\end{figure}

Figure 2 shows a high level overview of this project's workflow. Data is collected from Reddit using multiple PRAW instances, stored in a PostgreSQL database, and then models are trained using off the shelf sklearn models. As described above, this project has two main portions described below.

\subsection*{Part 2: Model training}
Scikit-learn was used to train classification models in order to make predictions. The two main models used were Logistic Regression and Random Forest Classifiers. During the model training process, the data undergoes a significant preprocessing step between being read from the database and being used in the model. A subset of the 27 time points are extracted to do training on, front page position representation for a post that did not make it to the front page is re-encoded, and the domain is extracted from the url and encoded using a one hot encoder. This produces a highly dimensional feature set, with \url{~}1500 features.

\section*{5. Software}
This project uses Python 3 for both data collection and model training, and PostgreSQL for data storage. The below table lists modules and other software used. \\

\begin{center}
	\begin{tabular}{| l | l | l |}
		\hline
		\textbf{My code} 		& \textbf{Python modules/software used} & \textbf{Description}\\
		\hline
		Reddit Crawler 			& multiprocessing.Process 				& Uses PRAW to scrape data from \\
								& PRAW 									& Reddit and inserts it into a \\
								& requests 								& PostgreSQL database.\\
								& pandas & \\
								& psycopg2 & \\
								& PostgreSQL & \\
		\hline
		Classification Models	& sqlalchemy 							& Uses Scikit-learn to train and \\
								& pandas 								& predict with classification \\
								& scikit-learn 							& models whether or not a post \\
								& imblearn 								& will rise to the front page.\\
		\hline
		Other					& matplotlib 							& These modules are used during \\
								& seaborn								& the data exploration and \\
								& collections.Counter 					& visualization process.\\
		\hline
	\end{tabular}
\end{center}

\section*{6. Experiments and Evaluation}
\ifx
\vspace{-3ex}
\textit{\scriptsize \indent Note: figures on next page}
\fi

Note: All models were trained using five time bins, unless otherwise noted.

Figures appear on page 5.
\subsection*{Part 1: Class imbalance}
Because such a small number of posts rise to the front page, there is significant class imbalance between posts that do and do not make it to the front page within one day. In order to overcome this, minority class oversamplers from imbalanced-learn.over\_sampling were tested, the imbalanced learning module counterpart to scikit-learn. Oversamplers tested were SMOTE, ADASYN, and RandomOverSampler. Using a RandomOverSampler in tandem with a random forest shows greatly improved prediction accuracy over the other methods. The results are summarized in Figure 3, with the dashed line representing the base rate.

\ifx
\begin{figure}[H]
\centering
\includegraphics[scale=0.39]{oversamp}
\caption{Performance of various oversamplers compared to baseline without.}
\end{figure}
\fi

\subsection*{Part 2: Impact of feature transformations}
The distribution of scores and number of comments was highly right skewed, which is typically fixed with a log or cube root transformation. Using a log transformation on this data is not possible, because there are many datapoints with a score and/or number of comments of zero. Therefore, a cube root transformation was used, which proves to be effective in improving classification accuracy for the random forest model, but is detrimental to the logistic regression model as shown in Figure 4, with the dashed line representing the base rate.

\ifx
\begin{figure}[H]
\centering
\includegraphics[scale=0.32]{transform}
\caption{Effect of using a cube root transform on numeric features.}
\end{figure}
\fi

\subsection*{Part 3: Exploring feature importance}
There are three main features being used: score, upvote\_ratio, and number of comments. The effect is not very large, but it is noticeable that the score feature contains a large portion of the predictive power, especially for the logistic regression model. In order to determine feature importance, a baseline model was trained with all three features, and then three new models were trained, each missing one of the three features.

\ifx
\begin{figure}[H]
\centering
\includegraphics[scale=0.32]{leave_one_out}
\caption{Performance when leaving one of the three main features out.}
\end{figure}
\fi

\subsection*{Part 4: Accuracy vs number of time bins}
Because 27 data points are being collected for each post over 24 hours, classification models can be trained on an any number of time bins, hypothetically with more bins having higher accuracy. This was tested by constructing a loop to preprocess an increasing number of bins(1-15), training a model, and making predictions for each number of bins. Figure 5 shows the accuracies collected from this experiment, and it is apparent that accuracy increases steadily with the number of bins used, although the logistic regression model begins to overfit and the test accuracy drops below baseline.

\ifx
\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{num_time_bins}
\caption{Performance with an increasing number of time bins used for training. Dashed line represents base rate.}
\end{figure}
\fi

\begin{figure}[H]
\begin{tabular}{cc}
	\includegraphics[scale=0.3]{oversamp} & \includegraphics[scale=0.3]{transform} \\
	(Fig. 3) Oversampling comparison & (Fig. 4) Effect of cube root transform \\[1cm]
	\includegraphics[scale=0.3]{leave_one_out} & \includegraphics[scale=0.25]{num_time_bins} \\
	(Fig. 5) Effect of removing features & (Fig. 6) Model accuracy with varying number of bins \\
\end{tabular}
\end{figure}

\section*{7. Notebook Description}
The Jupyter Notebook is broken up into five distinct sections. The first demonstrates briefly how data collection was done using PRAW, and the next four correspond to code used in the above four experiments. The complete code for the data collection system used is included in handler.py and worker.py, but is not included in the notebook. Cells pertaining to reading from or writing to the PostgreSQL database are commented out, and data loaded from pickled objects in the data folder.

In order to properly execute the data collection portion, a Reddit app must be created here: \url{https://www.reddit.com/prefs/apps/}.

\begin{enumerate}[itemsep=-2mm]
\item name: anything
\item description: optional
\item about url: anything
\item redirect uri: http://localhost:8080
\end{enumerate}

At the top of the notebook, Reddit authentication tokens must be changed to valid tokens, as described in PRAW documentation here: \url{https://praw.readthedocs.io/en/latest/getting_started/authentication.html#script-application}

\section*{8. Discussion and Conclusion}
The results of this project show that there is predictive power in these models, even for ones that train on a small subset of the time bins available. As seen in Figure 6, the baseline rate does not change until the fifth bin, and even then, by barely any amount. Bin 5 corresponds to 30 minute old posts, meaning once a post is 30 minutes old, the models described above can predict with 95\% accuracy whether or not the post will make it to the front page or not. 

One substantial obstacle that was present throughout the duration of this project was the fact that it was difficult to debug the data collection system. The system takes a full day to reach a stable state, because it has to accumulate an entire day's worth of posts in order to do so. Because of this, the system would sometimes go for many days before failing or showing signs of eventual failure. In the end, the system had to be limited to 500 simultaneous posts being tracked in order to bypass the multi-day debugging process and move onto training models.

There was also a lot to learn about using public API's works, for example OAuth2 authentication, how multiprocessing/threading interacts with authentication, and rate limiting. PRAW was able to abstract a lot of these things away, but it was very informative exploring PRAW's source code to see how it works. PRAW is able to examine http response headers for rate limiting information, which is used to determine whether or not it needs to postpone or slow down requests automatically. I had used Python's threading library before, but not multiprocessing. The added challenges in completely separated memory spaces were unfamiliar at first, eventually requiring a completely separate PRAW instance for each process after trying many variations of sharing PRAW instances between processes for reduced memory overhead.

Because I was collecting my own dataset, it was also difficult at first to figure out how to work on the project when away from home. There are a few options I considered: PostgreSQL on a remote machine (AWS, GCE), opening ports on my home network, making daily snapshots of the database to move onto my laptop, and running a VPN on my home network for a more secure connection. I ended up implementing the VPN option, which was a fun day project that does not fall within the scope of the class, but was interesting none the less.

Imbalanced-learn is a new module I had never used or heard of before this project. It proved very helpful, as I actually began implementing exactly what RandomOverSampler does before stumbling upon it. Past projects haven't had classes this imbalanced, so this was a new challenge I have been aware of but never experienced.

If this project were to continue, I would try to invest in a more modern machine, specifically with more memory to handle more simultaneous posts being tracked. I would also explore different kinds of features, possibly sentiment analysis on the top \url{~}10 comments on a post, as well as how effects like time of day effect likelihood of a post rising to the front page. Recurrent Neural Networks would also be interesting for use with a live system, which would allow the system to maintain a constantly updating list of new posts that the system thinks will rise to the front page later, possibly posting and updating the list on a subreddit dedicated to the predictions of this system.

\end{document}
