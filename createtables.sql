CREATE TABLE posts(
    uuid UUID PRIMARY KEY,
    post_id varchar,
    url varchar,
    subreddit varchar,
    username varchar,
    created_at integer,
    age integer,
    time_bin integer,
    score integer,
    upvote_ratio real,
    title varchar,
    selftext varchar,
    num_comments integer,
    gilded boolean,
    user_comment_karma integer,
    user_link_karma integer,
    frontpage_pos integer
);
