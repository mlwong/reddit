# Predicting Reddit Front Page Posts - UCI Data Science Senior Capstone Project  
Max Wong  
Winter/Spring '18

## Project Description
This project was done as a senior capstone project in the first graduating class in the new Data Science major. It aims to predict whether or not a new post on Reddit will reach the front page within 24 hours, based on models trained on the first ~2 hours of data.

### Data Collection
Because the models used need time series data on live posts, the first part of the project involved making a Reddit crawler using [PRAW](https://github.com/praw-dev/praw/blob/master/docs/index.rst) to collect data and store it in a PostgreSQL database. The crawler uses multiprocessing and has two parts, a handler and worker, which live in [/crawler](https://gitlab.com/mlwong/reddit/tree/master/crawler).

### Prediction models
Models were trained to classify whether or not a post will make it to the front page within 24 hours. Classification models were trained using Logistic Regression and Random Forest classifiers from [scikit-learn](https://github.com/scikit-learn/scikit-learn).

More in depth discussion of methods and results can be found in [/report](https://gitlab.com/mlwong/reddit/tree/master/report)